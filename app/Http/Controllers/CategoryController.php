<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ProductUa;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', 1)->get();
        return response()->json([
            'success' => true,
            'categories' => $categories
        ]);
    }

}
