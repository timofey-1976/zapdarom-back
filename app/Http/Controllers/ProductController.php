<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ProductUa;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json([
            'success' => true
        ]);
    }

    public function getProducts(Request $request)
    {

        $slug = Route::currentRouteName();
        $slugs = explode('_', $slug);
        $category = Category::where('slug', $slugs[0])->first();

        $categories = $category->subCategories->toArray();

        $ids = [];
        $ids = $this->getSubCatIds($categories);


        $products = (new \App\Models\Category)->getProducts($ids);
        $page = new Paginator($products, count($products), 20);
        return response()->json([
            'products' => $page,
            'categories' => $category->children
        ]);
    }

    public function getProduct($id)
    {
        $product = ProductUa::find($id);
        return response()->json([
            'product' => $product
        ]);
    }

    public function getSubCatIds($subCat)
    {
        $ids = [];
        foreach ($subCat as $category) {
            array_push($ids, $category['id']);
            if ($category['sub_categories'] && count($category['sub_categories'])) {
                $ids = array_merge($ids, $this->getSubCatIds($category['sub_categories']));
            }
        }
        return $ids;
    }
}
