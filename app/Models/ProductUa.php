<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductUa extends Model
{
    use HasFactory;
    public $table = 'products_ua';

}
