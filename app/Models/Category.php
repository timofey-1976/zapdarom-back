<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public $table = 'categories';

    public function productsUa()
    {
        return $this->belongsToMany('App\Models\ProductUa', 'products_to_category', 'category_id', 'product_id');
    }

    public function productsTrade()
    {
        return $this->belongsToMany('App\Models\ProductTrade', 'products_to_category', 'category_id', 'product_id');
    }

    public function productsSite()
    {
        return $this->belongsToMany('App\Models\ProductSite', 'products_to_category', 'category_id', 'product_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    public function subCategories()
    {
        return $this->children()->with('subCategories');
    }

    public function getProducts($ids)
    {
        $products = [];
        foreach ($ids as $id) {
            $temp = Category::find($id)->productsUa->toArray();
            $temp2 = Category::find($id)->productsTrade->toArray();
            $temp3 = Category::find($id)->productsSite->toArray();
            $products = array_merge($products, $temp, $temp2, $temp3);
        }
        return $products;
    }

}
