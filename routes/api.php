<?php

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => '/shop'
], function () {
    Route::get('/', [\App\Http\Controllers\CategoryController::class, 'index']);


    $main_categories = Category::where('parent_id', 0)->get();
    foreach ($main_categories as $category) {
        Route::get($category['slug'], [\App\Http\Controllers\ProductController::class, 'getProducts'])->name($category['slug'] . '_' . $category['id']);

        if ($category['has_child']) {
            $subcategories = Category::where('parent_id', $category['id'])->where('slug', 'not like', '')->get();
            foreach ($subcategories as $subcategory) {

                Route::get($category['slug'] . '/' . $subcategory['slug'], [\App\Http\Controllers\ProductController::class, 'getProducts'])->name($subcategory['slug'] . '_' . $subcategory['id']);
                if ($subcategory['has_child']) {
                    $sub_subcategories = Category::where('parent_id', $subcategory['id'])->where('slug', 'not like', '')->get();
                    foreach ($sub_subcategories as $sub_subcategory) {

                        Route::get($category['slug'] . '/' . $subcategory['slug'] . '/' . $sub_subcategory['slug'], [\App\Http\Controllers\ProductController::class, 'getProducts'])->name($sub_subcategory['slug'] . '_' . $sub_subcategory['id']);
                        if ($sub_subcategory['has_child']) {
                            $sub_sub_subcategories = Category::where('parent_id', $sub_subcategory['id'])->where('slug', 'not like', '')->get();
                            foreach ($sub_sub_subcategories as $sub_sub_subcategory) {

                                Route::get($category['slug'] . '/' . $subcategory['slug'] . '/' . $sub_subcategory['slug'] . '/' . $sub_sub_subcategory['slug'], [\App\Http\Controllers\ProductController::class, 'getProducts'])->name($sub_sub_subcategory['slug'] . '_' . $sub_sub_subcategory['id']);
                            }
                        }
                    }
                }
            }
        }
    }

    Route::get('/product/{id}', [\App\Http\Controllers\ProductController::class, 'getProduct']);

});
